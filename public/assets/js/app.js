'use strict';

angular
    .module('app', [
        'ngResource',
        'ngRoute'
    ])
    .value('globals', {
        version: ''
    })
    .run(function(globals, $rootScope){
        const tags = document.getElementsByTagName('meta');
        for (let i = 0; i < tags.length; i++) {
            switch (tags[i].getAttribute('name')) {
                case 'version':
                    globals.version = tags[i].getAttribute('content');
                    break;
                case 'app-name':
                    $rootScope.appname = tags[i].getAttribute('content');
                    break;
            }
        }
    })
    .config(function($httpProvider, $locationProvider, $routeProvider){
        $httpProvider.interceptors.push('preventCacheTemplate');

        $locationProvider.hashPrefix('');
        $locationProvider.html5Mode({
            enabled: false,
            requireBase: true
        });

        /**
         * Routes ngRoute
         */
        $routeProvider
            .when('/', {
                controller: 'MainController',
                controllerAs: 'vm',
                templateUrl: './views/dashboard.html'
            })
            .when('/article/:articleId', {
                controller: 'ViewArticleController',
                controllerAs: 'vm',
                templateUrl: './views/view-article.html'
            })
            .otherwise({
                redirectTo: '/'
            });
    })
    .controller('MainController', function(
        $resource,
        $rootScope
    ){
        var vm = this;

        vm.loader = true;

        vm.init = function() {
            $rootScope.title = 'title';
        };

    })
    .controller('ViewArticleController', function(
        $resource,
        $routeParams,
        $rootScope,
        $sce
    ){
        var vm = this;

        function callBackSuccess(response) {

            for (let i = 0; i < response.data.sections.length; i++) {
                if (response.data.sections[i]['name'] == 'VideoEmbedded') {
                    response.data.sections[i]['urlIframeYoutube'] = $sce.trustAsResourceUrl('https://www.youtube.com/embed/' + response.data.sections[i]['identifierExternal'])
                }
            }
            vm.article.data = response.data.article;
            vm.article.tags = response.data.tags;
            vm.article.sections = response.data.sections;

            $rootScope.title += vm.article.data.title;

            vm.loader = false;
        }

        function callBackError(error) {
            console.error(error);
        }

        function getDataArticle(articleId) {
            vm.loader = true;
            $resource('./696ecb2a60e2142aa86d924ffc5232be073d09ea81c6189797a637dcd92baf47/:articleId').get(
                {articleId: articleId},
                callBackSuccess,
                callBackError
            );
        }

        vm.article = {
            data: {},
            sections: [],
            tags: []
        };

        vm.support = 'php';

        vm.init = function () {
            $rootScope.title = 'title';
            getDataArticle($routeParams.articleId);
        };
    })
    .service('preventCacheTemplate', function (globals) {
        return {
            request: function (config) {
                if (config.url.indexOf('views') !== -1) {
                    config.url = config.url + '?version=' + globals.version;
                }
                return config;
            }
        }
    });
