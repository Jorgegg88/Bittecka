<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Tag;

use App\Domain\IdentifierEntity;
use App\Domain\Tag\Tag;
use App\Domain\Tag\TagRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class TagRepository
 * @package App\Infrastructure\Persistence\Tag
 */
class TagRepository extends DoctrineRepository implements TagRepositoryInterface
{
    /**
     * @param IdentifierEntity $articleId
     * @return array
     */
    public function findAllByArticleId(IdentifierEntity $articleId): array
    {
        $dql = '
            SELECT 
                t.name,
                t.id
            FROM App\\Domain\\Tag\\Tag t 
                JOIN App\\Domain\\Article\\Article a
            WHERE     
                a.id = :articleId';

        return $this->entityManager->createQuery($dql)->setParameter('articleId', $articleId->get())->getArrayResult();
    }

    /**
     * @param Tag $tag
     * @return Tag
     */
    public function save(Tag $tag): Tag
    {
        $this->entityManager->persist($tag);
        $this->entityManager->flush();

        return $tag;
    }

    /**
     * @param string $name
     * @return Tag|null
     */
    public function findOneByName(string $name): ?Tag
    {
        return $this->entityManager->getRepository(Tag::class)->findOneBy(array(
           'name'   =>  $name
        ));
    }
}