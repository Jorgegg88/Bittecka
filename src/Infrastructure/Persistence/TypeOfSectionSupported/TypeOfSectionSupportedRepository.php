<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\TypeOfSectionSupported;

use App\Domain\IdentifierEntity;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupported;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class TypeOfSectionSupportedRepository
 * @package App\Infrastructure\Persistence\TypeOfSectionSupported
 */
class TypeOfSectionSupportedRepository extends DoctrineRepository implements TypeOfSectionSupportedRepositoryInterface
{
    /**
     * @param IdentifierEntity $typeId
     * @return TypeOfSectionSupported|null
     */
    public function findOneById(IdentifierEntity $typeId): ?TypeOfSectionSupported
    {
        return $this->entityManager->find(TypeOfSectionSupported::class, $typeId->get());
    }

    /**
     * @return array|null
     */
    public function findAll(): ?array
    {
        return $this->entityManager->getRepository(TypeOfSectionSupported::class)->findAll();
    }
}