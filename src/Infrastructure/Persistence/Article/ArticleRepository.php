<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Article;

use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;
use App\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class ArticleRepository
 * @package App\Infrastructure\Persistence\Article
 */
class ArticleRepository extends DoctrineRepository implements ArticleRepositoryInterface
{
    /**
     * @param Article $article
     * @return string
     */
    public function save(Article $article): string
    {
        $this->entityManager->persist($article);
        $this->entityManager->flush();

        return $article->getId();
    }

    /**
     * @param IdentifierEntity $articleId
     * @return array|null
     * @throws NoResultException
     * @throws NonUniqueResultException
     */
    public function findOneHydratedArrayById(IdentifierEntity $articleId): ?array
    {
        $dql        =  'SELECT
                            a 
                            FROM 
                            App\\Domain\\Article\\Article a                    
                            WHERE a.id = :articleId';

        return  $this->entityManager->createQuery($dql)->setParameter('articleId', $articleId->get())->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param IdentifierEntity $articleId
     * @return Article|null
     */
    public function findOneById(IdentifierEntity $articleId): ?Article
    {
        return $this->entityManager->find(Article::class, $articleId->get());
    }

    /**
     * @param Article $article
     */
    public function delete(Article $article): void
    {
        $this->entityManager->remove($article);
        $this->entityManager->flush();
    }

    /**
     * @param int $page
     * @param int $numberRowsPerPage
     * @return array
     */
    public function findAll(int $page, int $numberRowsPerPage): array
    {
        $dql = 'SELECT a FROM App\\Domain\\Article\\Article a';

        return $this->entityManager->createQuery($dql)->setFirstResult($page - 1)->setMaxResults($numberRowsPerPage)->getResult();
    }
}