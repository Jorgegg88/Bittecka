<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\SectionTypeVideoEmbedded;


use App\Domain\IdentifierEntity;
use App\Domain\SectionType\SectionTypeRepositoryInterface;
use App\Domain\SectionTypeVideoEmbedded\SectionTypeVideoEmbedded;
use App\Domain\SectionTypeVideoEmbedded\SectionTypeVideoEmbeddedRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\AbstractQuery;


/**
 * Class SectionTypeVideoEmbeddedRepository
 * @package App\Infrastructure\Persistence\SectionTypeVideoEmbedded
 */
class SectionTypeVideoEmbeddedRepository extends DoctrineRepository implements SectionTypeRepositoryInterface, SectionTypeVideoEmbeddedRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(IdentifierEntity $sectionId): array
    {
        $dql = '
                SELECT 
                    sve.identifierExternal,
                    s.order,
                    s.id,
                    t.name
                 FROM 
                    App\\Domain\\SectionTypeVideoEmbedded\\SectionTypeVideoEmbedded sve
                 JOIN 
                    App\\Domain\\Section\\Section s
                 JOIN 
                    App\\Domain\\TypeOfSectionSupported\\TypeOfSectionSupported t  
                    WITH s.type = t
                 WHERE 
                    s.id = :sectionId';

        return $this->entityManager->createQuery($dql)->setParameter('sectionId', $sectionId->get())->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param SectionTypeVideoEmbedded $sectionVideoEmbedded
     * @return SectionTypeVideoEmbedded
     */
    public function save(SectionTypeVideoEmbedded $sectionVideoEmbedded): SectionTypeVideoEmbedded
    {
        $this->entityManager->persist($sectionVideoEmbedded);
        $this->entityManager->flush();

        return $sectionVideoEmbedded;
    }
}