<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Section;

use App\Domain\IdentifierEntity;
use App\Domain\Section\Section;
use App\Domain\Section\SectionRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class SectionRepository
 * @package App\Infrastructure\Persistence\Section
 */
class SectionRepository extends DoctrineRepository implements SectionRepositoryInterface
{
    /**
     * @param IdentifierEntity $articleId
     * @return array
     */
    public function findAllByArticleId(IdentifierEntity $articleId): array
    {
        $sectionsByType = array();
        $sections = $this->entityManager->getRepository(Section::class)->findBy(array('article' => $articleId->get()));

        foreach ($sections as $section) {
            $class = "App\\Infrastructure\\Persistence\\SectionType{$section->getType()->getName()}\\SectionType{$section->getType()->getName()}Repository";
            if (class_exists($class)) {
                $identifierEntity = new IdentifierEntity($section->getId());
                $sectionsByType[] = (new $class($this->entityManager))->findOneById($identifierEntity);
            }
        }
        return $sectionsByType;
    }
    /**
     * @param Section $section
     */
    public function delete(Section $section): void
    {
        $this->entityManager->remove($section);
        $this->entityManager->flush();
    }

    /**
     * @param IdentifierEntity $sectionId
     * @return Section|null
     */
    public function findOneById(IdentifierEntity $sectionId): ?Section
    {
        return $this->entityManager->find(Section::class, $sectionId->get());
    }

    /**
     * @param Section $section
     * @return Section|null
     */
    public function save(Section $section): ?section
    {
        $this->entityManager->persist($section);
        $this->entityManager->flush();

        if (!$section->getId()) {
            return null;
        }
        return $section;
    }
}