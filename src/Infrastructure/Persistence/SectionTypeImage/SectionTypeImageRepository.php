<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\SectionTypeImage;


use App\Domain\IdentifierEntity;
use App\Domain\SectionType\SectionTypeRepositoryInterface;
use App\Domain\SectionTypeImage\SectionTypeImage;
use App\Domain\SectionTypeImage\SectionTypeImageRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\AbstractQuery;

/**
 * Class SectionTypeImageRepository
 * @package App\Infrastructure\Persistence\SectionTypeImage
 */
class SectionTypeImageRepository extends DoctrineRepository implements SectionTypeRepositoryInterface, SectionTypeImageRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(IdentifierEntity $sectionId): array
    {
        $dql = '
                SELECT 
                    si.filename,
                    s.order,
                    s.id,
                    t.name
                 FROM 
                    App\\Domain\\SectionTypeImage\\SectionTypeImage si
                 JOIN 
                    App\\Domain\\Section\\Section s
                 JOIN 
                    App\\Domain\\TypeOfSectionSupported\\TypeOfSectionSupported t  
                    WITH s.type = t
                 WHERE 
                    s.id = :sectionId';

        return $this->entityManager->createQuery($dql)->setParameter('sectionId', $sectionId->get())->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param SectionTypeImage $sectionTypeImage
     * @return SectionTypeImage
     */
    public function save(SectionTypeImage $sectionTypeImage): SectionTypeImage
    {
        $this->entityManager->persist($sectionTypeImage);
        $this->entityManager->flush();

        return $sectionTypeImage;
    }
}