<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\SectionTypeText;


use App\Domain\IdentifierEntity;
use App\Domain\SectionType\SectionTypeRepositoryInterface;
use App\Domain\SectionTypeText\SectionTypeText;
use App\Domain\SectionTypeText\SectionTypeTextRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\AbstractQuery;

/**
 * Class SectionTypeTextRepository
 * @package App\Infrastructure\Persistence\SectionTypeText
 */
class SectionTypeTextRepository extends DoctrineRepository implements SectionTypeRepositoryInterface, SectionTypeTextRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(IdentifierEntity $sectionId): array
    {
        $dql = '
                SELECT 
                    st.text,
                    s.order,
                    s.id,
                    t.name
                 FROM 
                    App\\Domain\\SectionTypeText\\SectionTypeText st 
                 JOIN 
                    App\\Domain\\Section\\Section s
                 JOIN 
                    App\\Domain\\TypeOfSectionSupported\\TypeOfSectionSupported t  
                    WITH s.type = t
                 WHERE 
                    s.id = :sectionId';

        return $this->entityManager->createQuery($dql)->setParameter('sectionId', $sectionId->get())->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param SectionTypeText $sectionTypeText
     * @return SectionTypeText|null
     */
    public function save(SectionTypeText $sectionTypeText): ?SectionTypeText
    {
        $this->entityManager->persist($sectionTypeText);
        $this->entityManager->flush();

        return $sectionTypeText;
    }
}