<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\SectionTypeCode;


use App\Domain\IdentifierEntity;
use App\Domain\SectionType\SectionTypeRepositoryInterface;
use App\Domain\SectionTypeCode\SectionTypeCode;
use App\Domain\SectionTypeCode\SectionTypeCodeRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;
use Doctrine\ORM\AbstractQuery;

/**
 * Class SectionTypeCodeRepository
 * @package App\Infrastructure\Persistence\SectionTypeCode
 */
class SectionTypeCodeRepository extends DoctrineRepository implements SectionTypeRepositoryInterface, SectionTypeCodeRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return array
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findOneById(IdentifierEntity $sectionId): array
    {
        $dql = '
                SELECT 
                    sc.code,
                    s.order,
                    s.id,
                    t.name
                 FROM 
                    App\\Domain\\SectionTypeCode\\SectionTypeCode sc
                 JOIN 
                    App\\Domain\\Section\\Section s
                 JOIN 
                    App\\Domain\\TypeOfSectionSupported\\TypeOfSectionSupported t  
                    WITH s.type = t
                 WHERE 
                    s.id = :sectionId';

        return $this->entityManager->createQuery($dql)->setParameter('sectionId', $sectionId->get())->getSingleResult(AbstractQuery::HYDRATE_ARRAY);
    }

    /**
     * @param SectionTypeCode $sectionTypeCode
     * @return SectionTypeCode
     */
    public function save(SectionTypeCode $sectionTypeCode): SectionTypeCode
    {
        $this->entityManager->persist($sectionTypeCode);
        $this->entityManager->flush();

        return $sectionTypeCode;
    }
}