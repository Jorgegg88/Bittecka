<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\TempFileUploaded;


use App\Domain\TempFileUploaded\TempFileUploaded;
use App\Domain\TempFileUploaded\TempFileUploadedRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class TempFileUploadedRepository
 * @package App\Infrastructure\Persistence\TempFileUploaded
 */
class TempFileUploadedRepository extends DoctrineRepository implements TempFileUploadedRepositoryInterface
{
    /**
     * @param TempFileUploaded $tempFileUploaded
     * @return TempFileUploaded
     */
    public function save(TempFileUploaded $tempFileUploaded): TempFileUploaded
    {
        $this->entityManager->persist($tempFileUploaded);
        $this->entityManager->flush();

        return $tempFileUploaded;
    }
}