<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Technology;

use App\Domain\IdentifierEntity;
use App\Domain\Technology\Technology;
use App\Domain\Technology\TechnologyRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class TechnologyRepository
 * @package App\Infrastructure\Persistence\Technology
 */
class TechnologyRepository extends DoctrineRepository implements TechnologyRepositoryInterface
{
    /**
     * @param Technology $technology
     * @return Technology
     */
    public function save(Technology $technology): Technology
    {
        $this->entityManager->persist($technology);
        $this->entityManager->flush();

        return $technology;
    }

    /**
     * @param string $slug
     * @return Technology|null
     */
    public function findOneBySlug(string $slug): ?Technology
    {
        return $this->entityManager->getRepository(Technology::class)->findOneBy(array(
            'slug' => $slug
        ));
    }

    /**
     * @param IdentifierEntity $technologyId
     * @return Technology|null
     */
    public function findOneById(IdentifierEntity $technologyId): ?Technology
    {
        return $this->entityManager->find(Technology::class, $technologyId->get());
    }
}