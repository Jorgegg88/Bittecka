<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Category;


use App\Domain\Category\Category;
use App\Domain\Category\CategoryRepositoryInterface;
use App\Infrastructure\Persistence\DoctrineRepository;

/**
 * Class CategoryRepository
 * @package App\Infrastructure\Persistence\Category
 */
class CategoryRepository extends DoctrineRepository implements CategoryRepositoryInterface
{
    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category
    {
        $this->entityManager->persist($category);
        $this->entityManager->flush();

        return $category;
    }

    /**
     * @param string $name
     * @return Category|null
     */
    public function findOneBySlug(string $name): ?Category
    {
        return $this->entityManager->getRepository(Category::class)->findOneBy(array(
            'slug'  =>  $name
        ));
    }

    /**
     * @return array
     */
    public function findAll(): array
    {
        return $this->entityManager->getRepository(Category::class)->findAll();
    }
}