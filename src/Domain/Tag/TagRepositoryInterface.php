<?php
declare(strict_types=1);

namespace App\Domain\Tag;

use App\Domain\IdentifierEntity;

/**
 * Interface TagRepositoryInterface
 * @package App\Domain\Tag
 */
interface TagRepositoryInterface
{
    /**
     * @param Tag $tag
     * @return Tag
     */
    public function save(Tag $tag): Tag;

    /**
     * @param IdentifierEntity $articleId
     * @return array
     */
    public function findAllByArticleId(IdentifierEntity $articleId): array;

    /**
     * @param string $name
     * @return Tag|null
     */
    public function findOneByName(string $name): ?Tag;
}