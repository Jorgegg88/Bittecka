<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeText;


use App\Domain\SectionType\SectionType;

/**
 * Class SectionTypeText
 * @package App\Domain\SectionTypeText
 */
class SectionTypeText extends SectionType
{
    /**
     * @var string
     */
    private $text;

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param $text
     * @return $this
     */
    public function setText($text): self
    {
        $this->text = $text;

        return $this;
    }
}