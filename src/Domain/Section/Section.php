<?php
declare(strict_types=1);

namespace App\Domain\Section;

use App\Domain\Timestampable;

/**
 * Class Section
 * @package App\Domain\Section
 */
class Section
{
    use Timestampable;

    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $sort;

    /**
     * @var
     */
    private $article;

    /**
     * @var
     */
    private $type;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param $sort
     * @return $this
     */
    public function setSort($sort): self
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getArticle()
    {
        return $this->article;
    }

    /**
     * @param $article
     * @return $this
     */
    public function setArticle($article): self
    {
        $this->article = $article;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param $type
     * @return $this
     */
    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }
}