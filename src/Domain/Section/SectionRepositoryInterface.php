<?php
declare(strict_types=1);

namespace App\Domain\Section;

use App\Domain\IdentifierEntity;

/**
 * Interface SectionRepositoryInterface
 * @package App\Domain\Section
 */
interface SectionRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return Section|null
     */
    public function findOneById(IdentifierEntity $sectionId): ?Section;

    /**
     * @param Section $section
     * @return section|null
     */
    public function save(Section $section): ?section;

    /**
     * @param Section $section
     */
    public function delete(Section $section): void;

    /**
     * @param IdentifierEntity $articleId
     * @return array
     */
    public function findAllByArticleId(IdentifierEntity $articleId): array;
}