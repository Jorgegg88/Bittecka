<?php
declare(strict_types=1);

namespace App\Domain\Section;


use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Class SectionNotFoundException
 * @package App\Domain\Section
 */
class SectionNotFoundException extends DomainRecordNotFoundException
{
    protected $message = 'Section not found in delete request';
}