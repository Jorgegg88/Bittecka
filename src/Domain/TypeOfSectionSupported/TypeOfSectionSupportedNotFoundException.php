<?php
declare(strict_types=1);

namespace App\Domain\TypeOfSectionSupported;

use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Class TypeOfSectionSupportedNotFoundException
 * @package App\Domain\TypeOfSectionSupported
 */
class TypeOfSectionSupportedNotFoundException extends DomainRecordNotFoundException
{
    /**
     * @var string
     */
    protected $message = 'The type section requested is not supported';
}