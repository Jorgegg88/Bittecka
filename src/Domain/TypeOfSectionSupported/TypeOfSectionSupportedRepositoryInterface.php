<?php
declare(strict_types=1);

namespace App\Domain\TypeOfSectionSupported;

use App\Domain\IdentifierEntity;

/**
 * Interface TypeOfSectionSupportedRepositoryInterface
 * @package App\Domain\TypeOfSectionSupported
 */
interface TypeOfSectionSupportedRepositoryInterface
{
    /**
     * @param IdentifierEntity $typeId
     * @return TypeOfSectionSupported|null
     */
    public function findOneById(IdentifierEntity $typeId): ?TypeOfSectionSupported;

    /**
     * @return array|null
     */
    public function findAll(): ?array;
}