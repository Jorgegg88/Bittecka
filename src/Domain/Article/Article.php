<?php 
declare(strict_types=1);

namespace App\Domain\Article;

use App\Domain\Tag\Tag;
use App\Domain\Timestampable;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * Class Article
 */
class Article
{
    use Timestampable;

    /**
     * @var
     */
    private $id;

    /**
     * @var
     */
    private $title;

    /**
     * @var
     */
    private $owner;

    /**
     * @var
     */
    private $tags;

    /**
     * @var
     */
    private $categories;

    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param $id
     * @return $this
     */
    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return $this
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
    
    /**
     * @return ArrayCollection
     */
    public function getTags(): ArrayCollection
    {
        return $this->tags;
    }

    /**
     * @param Tag $tag
     * @return $this
     */
    public function addTag(Tag $tag): self
    {
        $this->tags->add($tag);

        return $this;
    }
}
