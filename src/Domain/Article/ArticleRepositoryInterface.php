<?php
declare(strict_types=1);

namespace App\Domain\Article;

use App\Domain\IdentifierEntity;

/**
 * Interface ArticleRepositoryInterface
 * @package App\Domain\Article
 */
interface ArticleRepositoryInterface
{
    /**
     * @param Article $article
     * @return string
     */
    public function save(Article $article): string;

    /**
     * @param IdentifierEntity $articleId
     * @return Article|null
     */
    public function findOneById(IdentifierEntity $articleId): ?Article;

    /**
     * @param Article $articleId
     */
    public function delete(Article $articleId): void;

    /**
     * @param int $page
     * @param int $numberRowsPerPage
     * @return array
     */
    public function findAll(int $page, int $numberRowsPerPage): array;
}