<?php
declare(strict_types=1);

namespace App\Domain\Article;


use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Class ArticleNotFoundException
 * @package App\Domain\Article
 */
final class ArticleNotFoundException extends DomainRecordNotFoundException
{
    /**
     * @var string
     */
    protected $message = 'The article you requested does not exist';
}