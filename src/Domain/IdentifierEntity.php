<?php
declare(strict_types=1);

namespace App\Domain;

/**
 * Class IdentifierEntity
 * @package App\Domain
 */
class IdentifierEntity
{
    /**
     * @var string
     */
    private $guid;

    /**
     * IdentifierEntity constructor.
     * @param string $guid
     */
    public function __construct(string $guid)
    {
        $this->guid = $guid;
    }

    /**
     * @return string
     */
    public function get(): string
    {
        return $this->guid;
    }
}