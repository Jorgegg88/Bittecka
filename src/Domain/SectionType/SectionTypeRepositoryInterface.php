<?php
declare(strict_types=1);

namespace App\Domain\SectionType;

use App\Domain\IdentifierEntity;

/**
 * Interface SectionTypeRepositoryInterface
 * @package App\Domain\SectionType
 */
interface SectionTypeRepositoryInterface
{
    /**
     * @param IdentifierEntity $sectionId
     * @return array
     */
    public function findOneById(IdentifierEntity $sectionId): array;
}