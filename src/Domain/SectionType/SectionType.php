<?php
declare(strict_types=1);

namespace App\Domain\SectionType;

/**
 * Class SectionType
 * @package App\Domain\SectionType
 */
class SectionType
{
    /**
     * @var
     */
    protected $section;

    /**
     * @return mixed
     */
    public function getSection()
    {
        return $this->section;
    }

    /**
     * @param $section
     * @return $this
     */
    public function setSection($section): self
    {
        $this->section = $section;

        return $this;
    }
}