<?php
declare(strict_types=1);

namespace App\Domain\TempFileUploaded;

use DateTime;

/**
 * Class TempFileUploaded
 * @package App\Domain\TempFileUploaded
 */
class TempFileUploaded
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var DateTime
     */
    private $uploadAt;

    /**
     * @var string
     */
    private $type;

    /**
     * @var bool
     */
    private $status;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUploadAt(): DateTime
    {
        return $this->uploadAt;
    }

    /**
     * @param DateTime $uploadAt
     * @return $this
     */
    public function setUploadAt(DateTime $uploadAt): self
    {
        $this->uploadAt = $uploadAt;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }
}