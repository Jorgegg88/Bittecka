<?php
declare(strict_types=1);


namespace App\Domain\TempFileUploaded;

/**
 * Interface TempFileUploadedRepositoryInterface
 * @package App\Domain\TempFileUploaded
 */
interface TempFileUploadedRepositoryInterface
{
    /**
     * @param TempFileUploaded $tempFileUploaded
     * @return TempFileUploaded
     */
    public function save(TempFileUploaded $tempFileUploaded): TempFileUploaded;
}