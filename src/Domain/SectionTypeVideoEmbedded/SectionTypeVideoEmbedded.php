<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeVideoEmbedded;


use App\Domain\SectionType\SectionType;

/**
 * Class SectionTypeVideoEmbedded
 * @package App\Domain\SectionTypeVideoEmbedded
 */
class SectionTypeVideoEmbedded extends SectionType
{
    /**
     * @var
     */
    private $identifierExternal;

    /**
     * @return mixed
     */
    public function getIdentifierExternal()
    {
        return $this->identifierExternal;
    }

    /**
     * @param mixed $identifierExternal
     */
    public function setIdentifierExternal($identifierExternal): void
    {
        $this->identifierExternal = $identifierExternal;
    }
}