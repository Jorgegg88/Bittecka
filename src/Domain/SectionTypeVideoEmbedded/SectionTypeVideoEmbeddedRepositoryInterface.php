<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeVideoEmbedded;

/**
 * Interface SectionTypeVideoEmbeddedRepositoryInterface
 * @package App\Domain\SectionTypeVideoEmbedded
 */
interface SectionTypeVideoEmbeddedRepositoryInterface
{
    /**
     * @param SectionTypeVideoEmbedded $sectionVideoEmbedded
     * @return SectionTypeVideoEmbedded
     */
    public function save(SectionTypeVideoEmbedded $sectionVideoEmbedded): SectionTypeVideoEmbedded;
}