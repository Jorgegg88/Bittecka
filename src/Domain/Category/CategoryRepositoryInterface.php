<?php
declare(strict_types=1);

namespace App\Domain\Category;

/**
 * Interface CategoryRepositoryInterface
 * @package App\Domain\Category
 */
interface CategoryRepositoryInterface
{
    /**
     * @param Category $category
     * @return Category
     */
    public function save(Category $category): Category;

    /**
     * @param string $name
     * @return Category|null
     */
    public function findOneBySlug(string $name): ?Category;

    /**
     * @return array
     */
    public function findAll(): array;
}