<?php
declare(strict_types=1);

namespace App\Domain\DomainException;

/**
 * Class DomainRecordNotFoundExceptio
 * @package App\Domain\DomainException
 */
class DomainRecordNotFoundException extends DomainException
{
}