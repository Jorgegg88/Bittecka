<?php
declare(strict_types=1);

namespace App\Domain\DomainException;

use Exception;
use Throwable;

/**
 * Class DomainException
 * @package App\Domain\DomainException
 */
abstract class DomainException extends Exception
{
}