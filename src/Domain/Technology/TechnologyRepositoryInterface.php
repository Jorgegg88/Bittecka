<?php
declare(strict_types=1);

namespace App\Domain\Technology;

use App\Domain\IdentifierEntity;

/**
 * Interface TechnologyRepositoryInterface
 * @package App\Domain\Technology
 */
interface TechnologyRepositoryInterface
{
    /**
     * @param Technology $technology
     * @return Technology
     */
    public function save(Technology $technology): Technology;

    /**
     * @param string $slug
     * @return Technology|null
     */
    public function findOneBySlug(string $slug): ?Technology;

    /**
     * @param IdentifierEntity $technologyId
     * @return Technology|null
     */
    public function findOneById(IdentifierEntity $technologyId): ?Technology;
}