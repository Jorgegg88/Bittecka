<?php
declare(strict_types=1);

namespace App\Domain\Technology;

use App\Domain\DomainException\DomainRecordNotFoundException;

/**
 * Class TechnologyNotFoundException
 * @package App\Domain\Technology
 */
class TechnologyNotFoundException extends DomainRecordNotFoundException
{
    protected $message = 'The technology not found';
}