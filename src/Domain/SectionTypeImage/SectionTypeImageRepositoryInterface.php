<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeImage;

/**
 * Interface SectionTypeImageRepositoryInterface
 * @package App\Domain\SectionTypeImage
 */
interface SectionTypeImageRepositoryInterface
{
    /**
     * @param SectionTypeImage $sectionTypeImage
     * @return SectionTypeImage
     */
    public function save(SectionTypeImage $sectionTypeImage): SectionTypeImage;
}