<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeImage;

use App\Domain\SectionType\SectionType;

/**
 * Class SectionTypeImage
 * @package App\Domain\SectionTypeImage
 */
class SectionTypeImage extends SectionType
{
    /**
     * @var
     */
    private $filename;

    /**
     * @return mixed
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param $filename
     * @return $this
     */
    public function setFilename($filename): self
    {
        $this->filename = $filename;

        return $this;
    }
}