<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeCode;

use App\Domain\SectionType\SectionType;
use App\Domain\Technology\Technology;

/**
 * Class SectionTypeCode
 * @package App\Domain\SectionTypeCode
 */
class SectionTypeCode extends SectionType
{
    /**
     * @var
     */
    private $technology;

    /**
     * @var string
     */
    private $code;

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    /**
     * @return Technology|null
     */
    public function getTechnology(): ?Technology
    {
        return $this->technology;
    }

    /**
     * @param Technology|null $technology
     * @return $this
     */
    public function setTechnology(?Technology $technology): self
    {
        $this->technology = $technology;

        return $this;
    }
}