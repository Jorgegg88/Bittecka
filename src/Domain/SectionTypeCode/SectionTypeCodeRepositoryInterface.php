<?php
declare(strict_types=1);

namespace App\Domain\SectionTypeCode;

/**
 * Interface SectionTypeCodeRepositoryInterface
 * @package App\Domain\SectionTypeCode
 */
interface SectionTypeCodeRepositoryInterface
{
    /**
     * @param SectionTypeCode $sectionTypeCode
     * @return SectionTypeCode
     */
    public function save(SectionTypeCode $sectionTypeCode): SectionTypeCode;
}