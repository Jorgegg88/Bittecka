<?php
declare(strict_types=1);

namespace App\Application\Services\Article;


use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;

/**
 * Class AddArticleService
 * @package App\Application\Services\Article
 */
class AddArticleService
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $repository;

    /**
     * AddArticleService constructor.
     * @param ArticleRepositoryInterface $repository
     */
    public function __construct(ArticleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param object $data
     * @return string
     * @throws \Exception
     */
    public function __invoke(object $data): string
    {
        $article = new Article();

        $article->setTitle($data->title);
        $article->setCreatedAt(new \DateTime());

        return $this->repository->save($article);
    }
}