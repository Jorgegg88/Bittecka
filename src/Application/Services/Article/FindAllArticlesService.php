<?php
declare(strict_types=1);

namespace App\Application\Services\Article;

use App\Domain\Article\ArticleRepositoryInterface;

/**
 * Class FindAllArticlesService
 * @package App\Application\Services\Article
 */
final class FindAllArticlesService
{
    private const NUMBER_ROWS_PER_PAGE = 35;

    /**
     * @var ArticleRepositoryInterface
     */
    private $repository;

    /**
     * AddTagToArticleService constructor.
     * @param ArticleRepositoryInterface $repository
     */
    public function __construct(ArticleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $page
     * @return array
     */
    public function __invoke(int $page): array
    {
        return $this->repository->findAll($page, self::NUMBER_ROWS_PER_PAGE);
    }
}