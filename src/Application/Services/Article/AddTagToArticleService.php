<?php
declare(strict_types=1);

namespace App\Application\Services\Article;

use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\Tag\Tag;

/**
 * Class AddTagToArticleService
 * @package App\Application\Services\Article
 */
final class AddTagToArticleService
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $repository;

    /**
     * AddTagToArticleService constructor.
     * @param ArticleRepositoryInterface $repository
     */
    public function __construct(ArticleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Article $article
     * @param Tag $tag
     */
    public function __invoke(Article $article, Tag $tag): void
    {
        foreach ($article->getTags() as $articleTag) {
            if ($articleTag->getId() === $tag->getId()) {
                return;
            }
        }
        $article->addTag($tag);
        $this->repository->save($article);
    }
}