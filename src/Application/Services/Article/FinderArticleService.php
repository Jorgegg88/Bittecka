<?php
declare(strict_types=1);

namespace App\Application\Services\Article;

use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;

/**
 * Class FinderArticleService
 * @package App\Application\Services\Article
 */
class FinderArticleService
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $repository;

    /**
     * FinderArticleService constructor.
     * @param ArticleRepositoryInterface $repository
     */
    public function __construct(ArticleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $articleId
     * @return Article|null
     */
    public function __invoke(IdentifierEntity $articleId): ?Article
    {
        return $this->repository->findOneById($articleId);
    }
}