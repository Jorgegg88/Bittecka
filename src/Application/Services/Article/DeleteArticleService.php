<?php
declare(strict_types=1);

namespace App\Application\Services\Article;

use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;

/**
 * Class DeleteArticleService
 * @package App\Application\Services\Article
 */
final class DeleteArticleService
{
    /**
     * @var ArticleRepositoryInterface
     */
    private $repository;

    /**
     * AddTagToArticleService constructor.
     * @param ArticleRepositoryInterface $repository
     */
    public function __construct(ArticleRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Article $article
     */
    public function __invoke(Article $article): void
    {
        $this->repository->delete($article);
    }
}