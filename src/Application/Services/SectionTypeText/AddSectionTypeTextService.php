<?php
declare(strict_types=1);

namespace App\Application\Services\SectionTypeText;

use App\Application\Services\SectionType\AddSectionTypeService;
use App\Domain\Section\Section;
use App\Domain\SectionTypeText\SectionTypeText;

/**
 * Class AddSectionTypeTextService
 * @package App\Application\Services\SectionType
 */
final class AddSectionTypeTextService extends AddSectionTypeService
{
    /**
     * @param Section $section
     * @param object $params
     * @return SectionTypeText|null
     */
    public function __invoke(Section $section, object $params): ?SectionTypeText
    {
        $sectionTypeText = new SectionTypeText();

        $sectionTypeText->setSection($section);
        $sectionTypeText->setText($params->text);

        return $this->repository->save($sectionTypeText);
    }
}