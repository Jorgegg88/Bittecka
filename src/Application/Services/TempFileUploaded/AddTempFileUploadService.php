<?php
declare(strict_types=1);

namespace App\Application\Services\TempFileUploaded;

use App\Domain\TempFileUploaded\TempFileUploaded;
use App\Domain\TempFileUploaded\TempFileUploadedRepositoryInterface;
use DateTime;
use RuntimeException;

/**
 * Class AddTempFileUploadService
 * @package App\Application\Services\TempFileUploaded
 */
final class AddTempFileUploadService
{
    /**
     * @var TempFileUploadedRepositoryInterface
     */
    private $repository;

    /**
     * AddTempFileUploadService constructor.
     * @param TempFileUploadedRepositoryInterface $repository
     */
    public function __construct(TempFileUploadedRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function __invoke(string $pathFilename, string $type): TempFileUploaded
    {
        $filename = $this->getFilenameFromPath($pathFilename);

        if (empty($filename)) {
            throw new RuntimeException("Cannot read filename from path: '{$pathFilename}'");
        }
        $tempFileUploaded = new TempFileUploaded();
        $tempFileUploaded->setFilename($filename);
        $tempFileUploaded->setType($type);
        $tempFileUploaded->setStatus(false);
        $tempFileUploaded->setUploadAt(new DateTime());

        return $this->repository->save($tempFileUploaded);
    }

    private function getFilenameFromPath(string $path): string
    {
        preg_match('/[a-fA-F0-9]+\.[a-z0-9]{1,4}$/', $path, $matches);

        return @$matches[0];
    }
}