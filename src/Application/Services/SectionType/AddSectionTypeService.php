<?php
declare(strict_types=1);

namespace App\Application\Services\SectionType;


use App\Domain\Section\Section;
use App\Domain\SectionType\SectionTypeRepositoryInterface;
use Psr\Container\ContainerInterface;

/**
 * Class AddSectionService
 * @package App\Application\Services\Section
 */
abstract class AddSectionTypeService
{
    /**
     * @var SectionTypeRepositoryInterface
     */
    protected $repository;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * AddSectionTypeService constructor.
     * @param SectionTypeRepositoryInterface $repository
     * @param ContainerInterface $container
     */
    public function __construct(SectionTypeRepositoryInterface $repository, ContainerInterface $container)
    {
        $this->repository = $repository;
        $this->container = $container;
    }

    /**
     * @param Section $section
     * @param object $params
     * @return mixed
     */
    abstract public function __invoke(Section $section, object $params);
}