<?php
declare(strict_types=1);

namespace App\Application\Services\TypeOfSectionSupported;

use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface;

/**
 * Class FindAllTypeOfSectionSupportedService
 * @package App\Application\Services\TypeOfSectionSupported
 */
final class FindAllTypeOfSectionSupportedService
{
    /**
     * @var TypeOfSectionSupportedRepositoryInterface
     */
    private $repository;

    /**
     * FinderTypeOfSectionSupported constructor.
     * @param TypeOfSectionSupportedRepositoryInterface $repository
     */
    public function __construct(TypeOfSectionSupportedRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array|null
     */
    public function __invoke(): ?array
    {
        return $this->repository->findAll();
    }
}