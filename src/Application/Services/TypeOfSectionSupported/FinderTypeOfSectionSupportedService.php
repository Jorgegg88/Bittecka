<?php
declare(strict_types=1);

namespace App\Application\Services\TypeOfSectionSupported;

use App\Domain\IdentifierEntity;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupported;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface;

/**
 * Class FinderTypeOfSectionSupported
 * @package App\Application\Services\TypeOfSectionSupported
 */
final class FinderTypeOfSectionSupportedService
{
    /**
     * @var TypeOfSectionSupportedRepositoryInterface
     */
    private $repository;

    /**
     * FinderTypeOfSectionSupported constructor.
     * @param TypeOfSectionSupportedRepositoryInterface $repository
     */
    public function __construct(TypeOfSectionSupportedRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $typeId
     * @return TypeOfSectionSupported|null
     */
    public function __invoke(IdentifierEntity $typeId): ?TypeOfSectionSupported
    {
        return $this->repository->findOneById($typeId);
    }
}