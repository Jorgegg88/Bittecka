<?php
declare(strict_types=1);

namespace App\Application\Services\Tag;

use App\Domain\IdentifierEntity;
use App\Domain\Tag\TagRepositoryInterface;

/**
 * Class FinderAllTagService
 * @package App\Application\Services\Tag
 */
class FinderAllTagService
{
    private $repository;

    public function __construct(TagRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $articleId
     * @return array|null
     */
    public function __invoke(IdentifierEntity $articleId): ?array
    {
        return $this->repository->findAllByArticleId($articleId);
    }
}