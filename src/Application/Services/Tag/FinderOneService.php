<?php
declare(strict_types=1);

namespace App\Application\Services\Tag;

use App\Domain\Tag\Tag;
use App\Domain\Tag\TagRepositoryInterface;

/**
 * Class FinderOneService
 * @package App\Application\Services\Tag
 */
final class FinderOneService
{
    /**
     * @var TagRepositoryInterface
     */
    private $repository;

    /**
     * FinderOneService constructor.
     * @param TagRepositoryInterface $repository
     */
    public function __construct(TagRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Tag|null
     */
    public function __invoke(string $name): ?Tag
    {
        return $this->repository->findOneByName(mb_strtolower($name));
    }
}