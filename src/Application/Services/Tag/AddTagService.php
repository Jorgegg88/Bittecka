<?php
declare(strict_types=1);

namespace App\Application\Services\Tag;

use App\Domain\IdentifierEntity;
use App\Domain\Tag\Tag;
use App\Domain\Tag\TagRepositoryInterface;

/**
 * Class AddTagService
 * @package App\Application\Services\Tag
 */
final class AddTagService
{
    /**
     * @var TagRepositoryInterface
     */
    public $repository;

    /**
     * AddTagService constructor.
     * @param TagRepositoryInterface $repository
     */
    public function __construct(TagRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Tag|null
     */
    public function __invoke(string $name): ?Tag
    {
        $tag = new Tag;
        $tag->setName(mb_strtolower($name));

        $tag = $this->repository->save($tag);

        if (!$tag->getId()) {
            return null;
        }
        return $tag;
    }
}