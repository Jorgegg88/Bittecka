<?php
declare(strict_types=1);

namespace App\Application\Services\SectionTypeImage;


use App\Application\Services\SectionType\AddSectionTypeService;
use App\Domain\SectionTypeImage\SectionTypeImage;
use App\Domain\Section\Section;

/**
 * Class SectionTypeImageService
 * @package App\Application\Services\SectionTypeImage
 */
final class AddSectionTypeImageService extends AddSectionTypeService
{
    /**
     * @param Section $section
     * @param object $params
     * @return mixed
     */
    public function __invoke(Section $section, object $params)
    {
        $sectionTypeImage = new SectionTypeImage();
        $sectionTypeImage->setFilename($params->filename);
        $sectionTypeImage->setSection($section);

        return $this->repository->save($sectionTypeImage);
    }
}