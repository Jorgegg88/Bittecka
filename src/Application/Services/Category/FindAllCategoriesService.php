<?php
declare(strict_types=1);

namespace App\Application\Services\Category;

use App\Domain\Category\CategoryRepositoryInterface;

/**
 * Class FindAllCategoriesService
 * @package App\Application\Services\Category
 */
final class FindAllCategoriesService
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $repository;

    /**
     * FindAllCategoriesService constructor.
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return array
     */
    public function __invoke(): array
    {
        return $this->repository->findAll();
    }
}