<?php
declare(strict_types=1);

namespace App\Application\Services\Category;

use App\Domain\Category\Category;
use App\Domain\Category\CategoryRepositoryInterface;

/**
 * Class FinderCategorySlugService
 * @package App\Application\Services\Category
 */
final class FinderCategorySlugService
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $repository;

    /**
     * FinderCategorySlugService constructor.
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Category|null
     */
    public function __invoke(string $name): ?Category
    {
        return $this->repository->findOneBySlug(mb_strtolower($name));
    }
}