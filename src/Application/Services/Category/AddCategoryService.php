<?php
declare(strict_types=1);

namespace App\Application\Services\Category;

use App\Domain\Category\Category;
use App\Domain\Category\CategoryRepositoryInterface;

/**
 * Class AddCategoryService
 * @package App\Application\Services\Category
 */
class AddCategoryService
{
    /**
     * @var CategoryRepositoryInterface
     */
    private $repository;

    /**
     * AddCategoryService constructor.
     * @param CategoryRepositoryInterface $repository
     */
    public function __construct(CategoryRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Category
     */
    public function __invoke(string $name): Category
    {
        $category = new Category();
        $category->setName($name);
        $category->setSlug(mb_strtolower($name));

        return $this->repository->save($category);
    }
}