<?php
declare(strict_types=1);

namespace App\Application\Services\Section;

use App\Domain\IdentifierEntity;
use App\Domain\Section\Section;
use App\Domain\Section\SectionRepositoryInterface;

/**
 * Class FinderSectionService
 * @package App\Application\Services\Section
 */
final class FinderSectionService
{
    /**
     * @var SectionRepositoryInterface
     */
    private $repository;

    /**
     * AddSectionService constructor.
     * @param SectionRepositoryInterface $repository
     */
    public function __construct(SectionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $sectionId
     * @return Section|null
     */
    public function __invoke(IdentifierEntity $sectionId): ?Section
    {
        return $this->repository->findOneById($sectionId);
    }
}