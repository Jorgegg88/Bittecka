<?php
declare(strict_types=1);

namespace App\Application\Services\Section;

use App\Domain\Article\Article;
use App\Domain\IdentifierEntity;
use App\Domain\Section\Section;
use App\Domain\Section\SectionRepositoryInterface;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupported;
use DateTime;

/**
 * Class AddSectionService
 * @package App\Application\Services\Section
 */
final class AddSectionService
{
    /**
     * @var SectionRepositoryInterface
     */
    private $repository;

    /**
     * AddSectionService constructor.
     * @param SectionRepositoryInterface $repository
     */
    public function __construct(SectionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /***
     * @param Article $article
     * @param int $order
     * @param TypeOfSectionSupported $type
     * @return Section|null
     * @throws \Exception
     */
    public function __invoke(Article $article, int $order, TypeOfSectionSupported $type): ?Section
    {
        $section = new Section();

        $section->setCreatedAt(new DateTime());
        $section->setSort($order);
        $section->setType($type);
        $section->setArticle($article);

        return $this->repository->save($section);
    }
}