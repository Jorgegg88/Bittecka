<?php
declare(strict_types=1);

namespace App\Application\Services\Section;

use App\Domain\IdentifierEntity;
use App\Domain\Section\SectionRepositoryInterface;

/**
 * Class FinderAllSectionService
 * @package App\Application\Services\Section
 */
class FinderAllSectionService
{
    /**
     * @var SectionRepositoryInterface
     */
    private $repository;

    /**
     * FinderAllSectionService constructor.
     * @param SectionRepositoryInterface $repository
     */
    public function __construct(SectionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $articleId
     * @return mixed
     */
    public function __invoke(IdentifierEntity $articleId)
    {
        return $this->repository->findAllByArticleId($articleId);
    }
}