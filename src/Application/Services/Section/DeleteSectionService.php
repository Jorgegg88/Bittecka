<?php
declare(strict_types=1);

namespace App\Application\Services\Section;

use App\Domain\Section\Section;
use App\Domain\Section\SectionRepositoryInterface;

/**
 * Class DeleteSectionService
 * @package App\Application\Services\Section
 */
final class DeleteSectionService
{
    /**
     * @var SectionRepositoryInterface
     */
    private $repository;

    /**
     * AddSectionService constructor.
     * @param SectionRepositoryInterface $repository
     */
    public function __construct(SectionRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Section $section
     */
    public function __invoke(Section $section): void
    {
        $this->repository->delete($section);
    }
}