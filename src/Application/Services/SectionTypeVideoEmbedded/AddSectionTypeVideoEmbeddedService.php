<?php
declare(strict_types=1);

namespace App\Application\Services\SectionTypeVideoEmbedded;


use App\Application\Services\SectionType\AddSectionTypeService;
use App\Domain\Section\Section;
use App\Domain\SectionTypeVideoEmbedded\SectionTypeVideoEmbedded;

/**
 * Class AddSectionTypeVideoEmbeddedService
 * @package App\Application\Services\SectionTypeVideoEmbedded
 */
class AddSectionTypeVideoEmbeddedService extends AddSectionTypeService
{
    /**
     * @param Section $section
     * @param object $params
     * @return SectionTypeVideoEmbedded
     */
    public function __invoke(Section $section, object $params): SectionTypeVideoEmbedded
    {
        $sectionTypeVideoEmbedded = new SectionTypeVideoEmbedded();
        $sectionTypeVideoEmbedded->setSection($section);
        $sectionTypeVideoEmbedded->setIdentifierExternal($params->identifier);

        return $this->repository->save($sectionTypeVideoEmbedded);
    }
}