<?php
declare(strict_types=1);

namespace App\Application\Services\Technology;

use App\Domain\Technology\Technology;
use App\Domain\Technology\TechnologyRepositoryInterface;

/**
 * Class FinderTechnologyService
 * @package App\Application\Services\Technology
 */
final class FinderTechnologyByNameService
{
    /**
     * @var TechnologyRepositoryInterface
     */
    private $repository;

    /**
     * AddTechnologyService constructor.
     * @param TechnologyRepositoryInterface $repository
     */
    public function __construct(TechnologyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Technology|null
     */
    public function __invoke(string $name): ?Technology
    {
        return $this->repository->findOneBySlug(trim(mb_strtolower($name)));
    }
}