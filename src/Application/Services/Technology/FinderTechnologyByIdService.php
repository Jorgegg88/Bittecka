<?php
declare(strict_types=1);

namespace App\Application\Services\Technology;

use App\Domain\IdentifierEntity;
use App\Domain\Technology\Technology;
use App\Domain\Technology\TechnologyRepositoryInterface;

/**
 * Class FinderTechnologyByIdService
 * @package App\Application\Services\Technology
 */
class FinderTechnologyByIdService
{
    /**
     * @var TechnologyRepositoryInterface
     */
    private $repository;

    /**
     * FindAllCategoriesService constructor.
     * @param TechnologyRepositoryInterface $repository
     */
    public function __construct(TechnologyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param IdentifierEntity $technologyId
     * @return Technology
     */
    public function __invoke(IdentifierEntity $technologyId): Technology
    {
        return $this->repository->findOneById($technologyId);
    }
}