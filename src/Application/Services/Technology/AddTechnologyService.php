<?php
declare(strict_types=1);

namespace App\Application\Services\Technology;

use App\Domain\IdentifierEntity;
use App\Domain\Technology\Technology;
use App\Domain\Technology\TechnologyRepositoryInterface;

/**
 * Class AddTechnologyService
 * @package App\Application\Services\Technology
 */
final class AddTechnologyService
{
    /**
     * @var TechnologyRepositoryInterface
     */
    private $repository;

    /**
     * AddTechnologyService constructor.
     * @param TechnologyRepositoryInterface $repository
     */
    public function __construct(TechnologyRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     * @return Technology
     */
    public function __invoke(string $name): Technology
    {
        $technology = new Technology();
        $technology->setName(trim($name));
        $technology->setSlug(mb_strtolower(trim($name)));

        $technology = $this->repository->save($technology);

        return $technology;
    }
}