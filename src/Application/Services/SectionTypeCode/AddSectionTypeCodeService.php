<?php
declare(strict_types=1);

namespace App\Application\Services\SectionTypeCode;


use App\Application\Services\SectionType\AddSectionTypeService;
use App\Application\Services\Technology\FinderTechnologyByIdService;
use App\Domain\IdentifierEntity;
use App\Domain\Section\Section;
use App\Domain\SectionTypeCode\SectionTypeCode;
use App\Domain\Technology\Technology;
use App\Domain\Technology\TechnologyNotFoundException;
use App\Domain\Technology\TechnologyRepositoryInterface;

/**
 * Class AddSectionTypeCodeService
 * @package App\Application\Services\SectionTypeCode
 */
class AddSectionTypeCodeService extends AddSectionTypeService
{
    /**
     * @param Section $section
     * @param object $params
     * @return SectionTypeCode
     * @throws TechnologyNotFoundException
     */
    public function __invoke(Section $section, object $params): SectionTypeCode
    {
        $sectionTypeCode = new SectionTypeCode();
        $sectionTypeCode->setSection($section);
        $sectionTypeCode->setCode($params->code);

        $finderTechnologyByIdService = new FinderTechnologyByIdService($this->container->get(TechnologyRepositoryInterface::class));
        $technology = $finderTechnologyByIdService(new IdentifierEntity($params->technology->id));

        if (gettype($technology) !== 'object') {
            throw new TechnologyNotFoundException();
        }
        $sectionTypeCode->setTechnology($technology);

        return $this->repository->save($sectionTypeCode);
    }
}