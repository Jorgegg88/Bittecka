<?php
declare(strict_types=1);

namespace App\Application\Handlers\Files;

/**
 * Class ImageProcessorHandler
 * @package App\Application\Handlers\Files
 */
class ImageProcessorHandler
{
    private const DEFAULT_QUALITY = 95;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var string
     */
    private $tempPathFilename;

    /**
     * ImageProcessorHandler constructor.
     * @param string $tempPathFilename
     * @param string $mimeType
     */
    public function __construct(string $tempPathFilename, string $mimeType)
    {
        $this->tempPathFilename = $tempPathFilename;
        $this->mimeType = $mimeType;
    }

    /**
     * @param string $targetFilePath
     * @return bool
     */
    public function createFile(string $targetFilePath): bool
    {
        $method = $this->getCallableResourceFactory();
        $resource = $method($this->tempPathFilename);

        $width = $this->getWidth();
        $height = $this->getHeight();

        $blankImage = imagecreatetruecolor($width, $height);

        imagecopyresampled($blankImage, $resource, 0, 0, 0, 0, $width, $height, $width, $height);

        imagejpeg($blankImage, $targetFilePath, self::DEFAULT_QUALITY);

        return (bool) file_exists($targetFilePath);
    }

    /**
     * @return int
     */
    private function getWidth(): int
    {
        return getImageSize($this->tempPathFilename)[0];
    }

    /**
     * @return int
     */
    private function getHeight(): int
    {
        return getImageSize($this->tempPathFilename)[1];
    }

    /**
     * @return callable
     */
    private function getCallableResourceFactory(): callable
    {
        switch ($this->mimeType) {
            case 'image/jpeg':
                return 'imagecreatefromjpeg';
                break;
            case 'image/png':
                return 'imagecreatefrompng';
                break;
            case 'image/gif':
                return 'imagecreatefromgif';
                break;
        }
    }
}