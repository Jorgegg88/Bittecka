<?php
declare(strict_types=1);

namespace App\Application\Handlers\Files;

/**
 * Class UploadFilesHandler
 * @package App\Application\Handlers\Files
 */
abstract class UploadFilesHandler
{
    /**
     * @var string
     */
    protected $directory;

    /**
     * UploadFilesHandler constructor.
     * @param string $directory
     */
    public function __construct(string $directory)
    {
        $this->directory = $directory;
    }

    /**
     * @param int $maxLength
     * @return string
     */
    protected function generateRandomFilename(int $maxLength = 65): string
    {
        $randomize = '';
        $hex = 'abcdef0123456789';

        while (strlen($randomize) < 65) {
            $randomize .= substr($hex, rand(0, strlen($hex)), 1);
        }
        return $randomize;
    }

    /**
     *
     */
    abstract function checkDirectoryExists(): void;
}