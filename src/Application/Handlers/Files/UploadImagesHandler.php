<?php
declare(strict_types=1);

namespace App\Application\Handlers\Files;


use RuntimeException;

/**
 * Class UploadImagesHandler
 * @package App\Application\Handlers\Files
 */
class UploadImagesHandler extends UploadFilesHandler
{
    /**
     * @var string
     */
    private $subDirectory;

    /**
     * @var array
     */
    private $typesAllowed = array(
        'image/jpeg',
        'image/gif',
        'image/png'
    );

    /**
     * UploadImagesHandler constructor.
     * @param string $directory
     * @param string $subDirectory
     */
    public function __construct(string $directory, string $subDirectory)
    {
        $this->subDirectory = $subDirectory;

        parent::__construct($directory);
    }

    /**
     * @param string $tempPathFilename
     * @param string $mimeType
     * @return string
     */
    public function save(string $tempPathFilename, string $mimeType): string
    {
        if (!$this->validateType($mimeType)) {
            throw new RuntimeException("The type '{$mimeType}' is not supported");
        }
        $this->checkDirectoryExists();

        $imageProcessorHandler = new ImageProcessorHandler($tempPathFilename, $mimeType);

        if (!$imageProcessorHandler->createFile($targetFilePath = $this->getTargetFilePath())) {
            throw new RuntimeException('Cannot create image file');
        }
        return $targetFilePath;
    }

    /**
     * @return string
     */
    private function getTargetFilePath(): string
    {
        return "{$this->directory}/{$this->subDirectory}/{$this->generateRandomFilename()}.jpeg";
    }

    /**
     * @param string $mimeType
     * @return bool
     */
    private function validateType(string $mimeType): bool
    {
        return (bool) in_array($mimeType, $this->typesAllowed);
    }

    /**
     * @inheritDoc
     */
    function checkDirectoryExists(): void
    {
        if (!is_dir($path = __DIR__ . "/../../../../public/{$this->directory}/{$this->subDirectory}")) {
            mkdir($path);
        }
    }
}