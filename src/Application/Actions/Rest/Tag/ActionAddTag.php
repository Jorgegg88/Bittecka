<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Tag;

use App\Application\Actions\Action;
use App\Application\Services\Article\AddTagToArticleService;
use App\Application\Services\Article\FinderArticleService;
use App\Application\Services\Tag\AddTagService;
use App\Application\Services\Tag\FinderOneService;
use App\Domain\Article\Article;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;
use App\Domain\Tag\Tag;
use App\Domain\Tag\TagRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;

/**
 * Class ActionAddTag
 * @package App\Application\Actions\Rest\Tag
 */
class ActionAddTag extends Action
{
    /**
     * @return Response
     * @throws HttpBadRequestException
     * @throws HttpNotFoundException
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();

        $tagRepository      = $this->container->get(TagRepositoryInterface::class);
        $articleRepository  = $this->container->get(ArticleRepositoryInterface::class);

        $finderOneService = new FinderOneService($tagRepository);
        $tag = $finderOneService->__invoke($formData->tagName);

        if (!$tag instanceof Tag) {
            $addTagService = new AddTagService($tagRepository);
            $tag =  $addTagService->__invoke($formData->tagName);
        }

        $finderArticleService = new FinderArticleService($articleRepository);
        $article = $finderArticleService->__invoke(new IdentifierEntity($formData->articleId));

        if (!$article instanceof Article) {
            throw new HttpNotFoundException();
        }

        $addTagToArticleService = new AddTagToArticleService($articleRepository);
        $addTagToArticleService->__invoke($article, $tag);

        return $this->respondWithData([
           'tagId'      =>  $tag->getId(),
           'tagName'    =>  $tag->getName()
        ]);
    }
}