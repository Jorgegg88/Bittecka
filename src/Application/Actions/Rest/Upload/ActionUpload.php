<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Upload;


use App\Application\Actions\Action;

/**
 * Class ActionUpload
 * @package App\Application\Actions\Rest\Upload
 */
abstract class ActionUpload extends Action
{
}