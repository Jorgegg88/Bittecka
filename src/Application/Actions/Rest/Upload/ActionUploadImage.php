<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Upload;


use App\Application\Handlers\Files\UploadImagesHandler;
use App\Application\Services\TempFileUploaded\AddTempFileUploadService;
use App\Domain\TempFileUploaded\TempFileUploadedRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionUploadImage
 * @package App\Application\Actions\Rest\Upload
 */
class ActionUploadImage extends ActionUpload
{
    /**
     * @var string
     */
    private $typeFileToUpload = 'image';

    /**
     * @inheritDoc
     */
    protected function action(): Response
    {
        if (!$this->validateInputData($files = $this->getFiles())) {
            throw new HttpBadRequestException($this->request);
        }
        $image = $files[$this->typeFileToUpload];

        $uploadImagesHandler = new UploadImagesHandler($this->container->get('settings')['uploads_dir'], $this->typeFileToUpload);
        $pathFilename = $uploadImagesHandler->save($image['tmp_name'], $image['type']);

        $addTempFileUploadRepository = new AddTempFileUploadService($this->container->get(TempFileUploadedRepositoryInterface::class));
        $addTempFileUploadRepository($pathFilename, $this->typeFileToUpload);

        return $this->respondWithData([
            'pathFilename' => $pathFilename
        ]);
    }

    /**
     * @param array|null $files
     * @return bool
     */
    private function validateInputData(?array $files): bool
    {
        if (empty($files)) {
            return false;
        }
        if (!isset($files[$this->typeFileToUpload])) {
            return false;
        }
        if ($files['image']['error'] !== 0) {
            return false;
        }
        return true;
    }
}