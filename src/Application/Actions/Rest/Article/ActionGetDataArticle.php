<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Article;


use App\Application\Actions\Action;
use App\Application\Services\Article\FinderArticleService;
use App\Application\Services\Section\FinderAllSectionService;
use App\Application\Services\Tag\FinderAllTagService;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;
use App\Domain\Section\SectionRepositoryInterface;
use App\Domain\Tag\TagRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class ActionGetDataArticle
 * @package App\Application\Actions\Rest\Article
 */
class ActionGetDataArticle extends Action
{
    /**
     * @return Response
     */
    protected function action(): Response
    {
        $articleId = new IdentifierEntity($this->args['articleId']);

        $finderArticleService = new FinderArticleService($this->container->get(ArticleRepositoryInterface::class));
        $finderAllTagService  = new FinderAllTagService($this->container->get(TagRepositoryInterface::class));
        $finderAllSectionService = new FinderAllSectionService($this->container->get(SectionRepositoryInterface::class));

        $article = $finderArticleService->__invoke($articleId);
        $tags    = $finderAllTagService->__invoke($articleId);
        $sections = $finderAllSectionService->__invoke($articleId);

        return $this->respondWithData(array(
            'article' =>  $article,
            'tags'    =>  $tags,
            'sections' => $sections
        ));
    }
}