<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Article;


use App\Application\Actions\Action;
use App\Application\Services\Article\AddArticleService;
use App\Domain\Article\ArticleRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionAddArticle
 * @package App\Application\Actions\Rest
 */
class ActionAddArticle extends Action
{
    /**
     * @return Response
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $addArticleService = new AddArticleService($this->container->get(ArticleRepositoryInterface::class));

        $articleId = $addArticleService->__invoke($this->getFormData());

        return $this->respondWithData(array(
            'articleId' => $articleId
        ));
    }
}