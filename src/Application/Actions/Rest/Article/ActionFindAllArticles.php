<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Article;


use App\Application\Actions\Action;
use App\Application\Services\Article\FindAllArticlesService;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionFindArticles
 * @package App\Application\Actions\Rest\Article
 */
class ActionFindAllArticles extends Action
{
    /**
     * @return Response
     */
    protected function action(): Response
    {
        $page = (int) $this->args['page'];

        $findAllArticlesService = new FindAllArticlesService($this->container->get(ArticleRepositoryInterface::class));
        $articles = $findAllArticlesService($page);

        return $this->respondWithData([
            'articles' => $articles
        ]);
    }
}