<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Article;


use App\Application\Actions\Action;
use App\Application\Services\Article\DeleteArticleService;
use App\Application\Services\Article\FinderArticleService;
use App\Domain\Article\ArticleNotFoundException;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class ActionDeleteArticle
 * @package App\Application\Actions\Rest\Article
 */
class ActionDeleteArticle extends Action
{
    /**
     * @return Response
     * @throws ArticleNotFoundException
     */
    protected function action(): Response
    {
        $articleId = new IdentifierEntity($this->args['articleId']);

        $articleRepository = $this->container->get(ArticleRepositoryInterface::class);

        $finderArticleService = new FinderArticleService($articleRepository);
        $article = $finderArticleService($articleId);

        if (gettype($article) !== 'object') {
            throw new ArticleNotFoundException();
        }

        $deleteArticleService = new DeleteArticleService($articleRepository);
        $deleteArticleService($article);

        return $this->respondWithData([
            'deleted' => true
        ]);
    }
}