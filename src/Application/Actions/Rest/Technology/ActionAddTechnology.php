<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Technology;


use App\Application\Actions\Action;
use App\Application\Services\Technology\AddTechnologyService;
use App\Application\Services\Technology\FinderTechnologyByNameService;
use App\Domain\Technology\TechnologyRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionAddTechnology
 * @package App\Application\Actions\Rest\Technology
 */
class ActionAddTechnology extends Action
{
    /**
     * @return Response
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();

        $technologyRepository = $this->container->get(TechnologyRepositoryInterface::class);

        if (!$this->validateFormData($formData)) {
            throw new HttpBadRequestException($this->request);
        }

        $finderTechnology = new FinderTechnologyByNameService($technologyRepository);
        $technology = $finderTechnology($formData->name);

        if (gettype($technology) !== 'object') {
            $addTechnologyService = new AddTechnologyService($technologyRepository);
            $technology = $addTechnologyService($formData->name);
        }

        return $this->respondWithData([
           'technologyId' => $technology->getId(),
           'technologyName' => $technology->getName()
        ]);
    }

    /**
     * @return bool
     */
    private function validateFormData(object $formData): bool
    {
        if (gettype($formData) !== 'object') {
            return false;
        }
        if (!isset($formData->name)) {
            return false;
        }
        if (empty($formData->name)) {
            return false;
        }
        return true;
    }
}