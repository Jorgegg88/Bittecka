<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\TypeOfSectionSupported;

use App\Application\Actions\Action;
use App\Application\Services\TypeOfSectionSupported\FindAllTypeOfSectionSupportedService;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class ActionGetTypeOfSectionSupported
 * @package App\Application\Actions\Rest\TypeOfSectionSupported
 */
class ActionGetTypeOfSectionSupported extends Action
{
    /**
     * @return Response
     */
    protected function action(): Response
    {
        $findAllTypeOfSectionSupported = new FindAllTypeOfSectionSupportedService($this->container->get(TypeOfSectionSupportedRepositoryInterface::class));
        $typeOfSectionSupportedList = $findAllTypeOfSectionSupported->__invoke();

        return $this->respondWithData([
           'typeOfSectionSupported' => $typeOfSectionSupportedList
        ]);
    }
}