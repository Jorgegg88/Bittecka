<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Category;


use App\Application\Actions\Action;
use App\Application\Services\Category\AddCategoryService;
use App\Application\Services\Category\FinderCategorySlugService;
use App\Domain\Category\Category;
use App\Domain\Category\CategoryRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionAddCategory
 * @package App\Application\Actions\Rest
 */
class ActionAddCategory extends Action
{
    /**
     * @return Response
     * @throws HttpBadRequestException
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();

        $categoryRepository = $this->container->get(CategoryRepositoryInterface::class);
        $finderCategorySlugService = new FinderCategorySlugService($categoryRepository);
        $category = $finderCategorySlugService->__invoke($formData->name);

        if (!$category instanceof Category) {
            $addCategoryService = new AddCategoryService($categoryRepository);
            $category = $addCategoryService->__invoke(trim($formData->name));
        }

        return $this->respondWithData(array(
            'categoryId'    =>  $category->getId(),
            'categoryName'  =>  $category->getName()
        ));
    }
}