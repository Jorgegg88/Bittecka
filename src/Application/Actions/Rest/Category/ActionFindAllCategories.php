<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Category;


use App\Application\Actions\Action;
use App\Application\Services\Category\FindAllCategoriesService;
use App\Domain\Category\CategoryRepositoryInterface;
use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Exception\HttpBadRequestException;

/**
 * Class ActionFindAllCategories
 * @package App\Application\Actions\Rest\Category
 */
class ActionFindAllCategories extends Action
{
    /**
     * @return Response
     */
    protected function action(): Response
    {
        $finderAllService = new FindAllCategoriesService($this->container->get(CategoryRepositoryInterface::class));
        $categories = $finderAllService();

        return $this->respondWithData([
            'categories' => $categories
        ]);
    }
}