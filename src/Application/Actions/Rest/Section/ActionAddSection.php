<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Section;

use App\Application\Actions\Action;
use App\Application\Services\Article\FinderArticleService;
use App\Application\Services\Section\AddSectionService;
use App\Application\Services\TypeOfSectionSupported\FinderTypeOfSectionSupportedService;
use App\Domain\Article\Article;
use App\Domain\Article\ArticleNotFoundException;
use App\Domain\Article\ArticleRepositoryInterface;
use App\Domain\IdentifierEntity;
use App\Domain\Section\Section;
use App\Domain\Section\SectionRepositoryInterface;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupported;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedNotFoundException;
use App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use RuntimeException;


/**
 * Class ActionAddSection
 * @package App\Application\Actions\Rest\Section
 */
class ActionAddSection extends Action
{
    /**
     * @return Response
     * @throws ArticleNotFoundException
     * @throws TypeOfSectionSupportedNotFoundException
     * @throws \Slim\Exception\HttpBadRequestException
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();

        $finderArticleService = new FinderArticleService($this->container->get(ArticleRepositoryInterface::class));
        $article = $finderArticleService->__invoke(new IdentifierEntity($formData->articleId));

        if (!$article instanceof Article) {
            throw new ArticleNotFoundException();
        }

        $finderTypeOfSectionSupported = new FinderTypeOfSectionSupportedService($this->container->get(TypeOfSectionSupportedRepositoryInterface::class));
        $typeOfSectionSupported = $finderTypeOfSectionSupported->__invoke(new IdentifierEntity($formData->typeId));

        if (!$typeOfSectionSupported instanceof TypeOfSectionSupported) {
            throw new TypeOfSectionSupportedNotFoundException();
        }

        $addSectionService = new AddSectionService($this->container->get(SectionRepositoryInterface::class));

        $section = $addSectionService->__invoke(
            $article,
            (int) $formData->order,
            $typeOfSectionSupported
        );

        if (!$section instanceof Section) {
            throw new RuntimeException('Cannot save section');
        }

        $sectionTypeName = $typeOfSectionSupported->getName();
        $serviceTypeSectionName = "App\\Application\\Services\\SectionType{$sectionTypeName}\\AddSectionType{$sectionTypeName}Service";

        if (!class_exists($serviceTypeSectionName)) {
            throw new RuntimeException("The class {$serviceTypeSectionName} not has yet implement");
        }

        if (!$this->container->has($repositoryIdentifer = "App\\Domain\\SectionType{$sectionTypeName}\\SectionType{$sectionTypeName}RepositoryInterface")) {
            throw new RuntimeException("Cannot find the repository {$repositoryIdentifer} in container list");
        }
        $repositoryTypeSection = $this->container->get($repositoryIdentifer);

        $sectionTypeService = new $serviceTypeSectionName($repositoryTypeSection, $this->container);
        $sectionTypeService->__invoke($section, $formData->params);

        return $this->respondWithData(['sectionId'  =>  $section->getId()]);
    }
}