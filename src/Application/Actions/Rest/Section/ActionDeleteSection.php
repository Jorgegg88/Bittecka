<?php
declare(strict_types=1);

namespace App\Application\Actions\Rest\Section;

use App\Application\Actions\Action;
use App\Application\Services\Section\DeleteSectionService;
use App\Application\Services\Section\FinderSectionService;
use App\Domain\IdentifierEntity;
use App\Domain\Section\SectionNotFoundException;
use App\Domain\Section\SectionRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;

/**
 * Class ActionDeleteSection
 * @package App\Application\Actions\Rest\Section
 */
class ActionDeleteSection extends Action
{
    /**
     * @return Response
     * @throws SectionNotFoundException
     */
    protected function action(): Response
    {
        /**
         * TODO: Validate GUID
         */
        $sectionId = new IdentifierEntity($this->args['sectionId']);
        $sectionRepository = $this->container->get(SectionRepositoryInterface::class);

        $finderSectionService = new FinderSectionService($sectionRepository);
        $section = $finderSectionService($sectionId);

        if ($section === null) {
            throw new SectionNotFoundException();
        }

        $deleteSectionService = new DeleteSectionService($sectionRepository);
        $deleteSectionService($section);

        return $this->respondWithData([
            'deleted' => true
        ]);
    }
}