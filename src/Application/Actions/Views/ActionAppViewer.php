<?php
declare(strict_types=1);

namespace App\Application\Actions\Views;

use App\Domain\DomainException\DomainRecordNotFoundException;
use Psr\Http\Message\ResponseInterface as Response;
use Slim\Views\PhpRenderer;

/**
 * Class ActionAppViewer
 * @package App\Application\Actions\Views
 */
class ActionAppViewer extends ActionViewProvider
{
    /**
     * @var string
     */
    private $filename = 'app';

    /**
     * @return Response
     */
    protected function action(): Response
    {
        $renderer = $this->container->get(PhpRenderer::class);
        $renderer->setAttributes(array(
            'version'   =>  $this->container->get('settings')['version'],
            'appname'   =>  $this->container->get('settings')['appname']
        ));

        return $renderer->render($this->response, "{$this->filename}.{$this->extensionFile}");
    }
}