<?php
declare(strict_types=1);

namespace App\Application\Actions\Views;

use App\Application\Actions\Action;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Class ActionViewProvider
 * @package App\Application\Actions
 */
abstract class ActionViewProvider extends Action
{
    /**
     * @var string
     */
    protected $extensionFile = 'phtml';
}