<?php
declare(strict_types=1);

use App\Domain\Article\ArticleRepositoryInterface,
    App\Domain\Category\CategoryRepositoryInterface,
    App\Domain\Section\SectionRepositoryInterface,
    App\Domain\SectionTypeImage\SectionTypeImageRepositoryInterface,
    App\Domain\SectionTypeText\SectionTypeTextRepositoryInterface,
    App\Domain\Tag\TagRepositoryInterface,
    App\Domain\TypeOfSectionSupported\TypeOfSectionSupportedRepositoryInterface,
    App\Infrastructure\Persistence\Article\ArticleRepository,
    App\Infrastructure\Persistence\Category\CategoryRepository,
    App\Infrastructure\Persistence\Section\SectionRepository,
    App\Infrastructure\Persistence\SectionTypeImage\SectionTypeImageRepository,
    App\Infrastructure\Persistence\SectionTypeText\SectionTypeTextRepository,
    App\Infrastructure\Persistence\Tag\TagRepository,
    App\Infrastructure\Persistence\TypeOfSectionSupported\TypeOfSectionSupportedRepository,
    DI\ContainerBuilder,
    Doctrine\ORM\EntityManager,
    Psr\Container\ContainerInterface,
    App\Domain\SectionTypeVideoEmbedded\SectionTypeVideoEmbeddedRepositoryInterface,
    App\Infrastructure\Persistence\SectionTypeVideoEmbedded\SectionTypeVideoEmbeddedRepository,
    App\Domain\SectionTypeCode\SectionTypeCodeRepositoryInterface,
    App\Infrastructure\Persistence\SectionTypeCode\SectionTypeCodeRepository,
    App\Domain\Technology\TechnologyRepositoryInterface,
    App\Infrastructure\Persistence\Technology\TechnologyRepository,
    App\Domain\TempFileUploaded\TempFileUploadedRepositoryInterface,
    App\Infrastructure\Persistence\TempFileUploaded\TempFileUploadedRepository;

return static function (ContainerBuilder $containerBuilder) {

    $containerBuilder->addDefinitions([
        ArticleRepositoryInterface::class => static function (ContainerInterface $c) {
            return new ArticleRepository($c->get(EntityManager::class));
        },
        TagRepositoryInterface::class => static function (ContainerInterface $c) {
            return new TagRepository($c->get(EntityManager::class));
        },
        SectionRepositoryInterface::class => static function (ContainerInterface $c) {
            return new SectionRepository($c->get(EntityManager::class));
        },
        CategoryRepositoryInterface::class => static function (ContainerInterface $c) {
            return new CategoryRepository($c->get(EntityManager::class));
        },
        TypeOfSectionSupportedRepositoryInterface::class => static function (ContainerInterface $c) {
            return new TypeOfSectionSupportedRepository($c->get(EntityManager::class));
        },
        SectionTypeTextRepositoryInterface::class => static function (ContainerInterface $c) {
            return new SectionTypeTextRepository($c->get(EntityManager::class));
        },
        SectionTypeImageRepositoryInterface::class => static function (ContainerInterface $c) {
            return new SectionTypeImageRepository($c->get(EntityManager::class));
        },
        SectionTypeVideoEmbeddedRepositoryInterface::class => static function (ContainerInterface $c) {
            return new SectionTypeVideoEmbeddedRepository($c->get(EntityManager::class));
        },
        SectionTypeCodeRepositoryInterface::class => static function (ContainerInterface $c) {
            return new SectionTypeCodeRepository($c->get(EntityManager::class));
        },
        TechnologyRepositoryInterface::class => static function (ContainerInterface $c) {
            return new TechnologyRepository($c->get(EntityManager::class));
        },
        TempFileUploadedRepositoryInterface::class => static function (ContainerInterface $c) {
            return new TempFileUploadedRepository($c->get(EntityManager::class));
        }
    ]);
};
