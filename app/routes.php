<?php
declare(strict_types=1);

use App\Application\Actions\Rest\Article\ActionAddArticle;
use App\Application\Actions\Rest\Article\ActionDeleteArticle;
use App\Application\Actions\Rest\Article\ActionFindAllArticles;
use App\Application\Actions\Rest\Article\ActionGetDataArticle;
use App\Application\Actions\Rest\Category\ActionFindAllCategories;
use App\Application\Actions\Rest\Section\ActionDeleteSection;
use App\Application\Actions\Rest\Tag\ActionAddTag;
use App\Application\Actions\Rest\Technology\ActionAddTechnology;
use App\Application\Actions\Rest\TypeOfSectionSupported\ActionGetTypeOfSectionSupported;
use App\Application\Actions\Rest\Upload\ActionUploadImage;
use App\Application\Actions\Views\ActionAppViewer;
use App\Application\Actions\Rest\Category\ActionAddCategory;
use App\Application\Actions\Rest\Section\ActionAddSection;
use Slim\App;

return static function (App $app) {

        $app->get('/', new ActionAppViewer($app->getContainer()));

        /**
         * New Article
         */
        $app->post('/3967e130269c8403f925d79d9dc44887eec465649b4505ff4b8e31f21c04fdbc',
            new ActionAddArticle($app->getContainer())
        );

        /**
         * Get article data
         */
        $app->get('/696ecb2a60e2142aa86d924ffc5232be073d09ea81c6189797a637dcd92baf47/{articleId}',
            new ActionGetDataArticle($app->getContainer())
        );

        /**
         * Create Category
         */
        $app->post('/d401313a1839d6d02a7a6144ae6fa574281300473a97acf82dae96971c58c4fd',
            new ActionAddCategory($app->getContainer())
        );

        /**
         * Create new section in the article
         */
        $app->post('/892280bd316034dc751bce6ad6878975acd4bb3d42579f9d709f03a5d9ebbc21',
              new ActionAddSection($app->getContainer())
        );

        /**
         * Create new Tag for Article existing
         */
        $app->post('/dac78d3a0f8db9eb501de5c77890b6a65ca40055f1894d403d72f1e98a34e8bb',
            new ActionAddTag($app->getContainer())
        );

        /**
         * Create a new technology
         */
        $app->post('/99830dc32cd1fe162e9bbb69838a2166bd8d038a23785337085e27420fd1b7d5',
            new ActionAddTechnology($app->getContainer())
        );

        /**
         * Get articles
         */
        $app->get('/9c5e34a9520d803d1571fffd2b71df19780ed60a731e896f54240ff8d5c6abc6/{page}',
            new ActionFindAllArticles($app->getContainer())
        );

        /**
         * Delete section
         */
        $app->delete('/eab947b9719c6f6446614f9c07cfbe64efca6ff83d2a8a0b982f87778ad18c98/{sectionId}',
            new ActionDeleteSection($app->getContainer())
        );

        /**
         * Get type sections available
         */
        $app->get('/4943165ec98996e37dd3a3111ef313a6f6353f96aabd0cd1b73bb320ec622ecc',
            new ActionGetTypeOfSectionSupported($app->getContainer())
        );

        /**
         * Get all categories
         */
        $app->get('/1888447213efa120c2d6da84a369daddc6a1404f519f637e90113ef697021105',
            new ActionFindAllCategories($app->getContainer())
        );

        /**
         * Delete article
         */
        $app->delete('/340e2790c5be97d28c3f37f71bbe0bbd78d5e62dd6dab08fec6a29f76f33ca40/{articleId}',
            new ActionDeleteArticle($app->getContainer())
        );

        /**
         * Upload Image
         */
        $app->post('/117b871d4ebd036307f55a73f8d126735908744af959d85737ef3e74e3d2a549',
            new ActionUploadImage($app->getContainer())
        );
};
