<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Monolog\Logger;

return static function (ContainerBuilder $containerBuilder) {

    $containerBuilder->addDefinitions([
        'settings' => [
            'appname'   =>  'Bittecka',
            'version'   =>  '1.0.0',
            'uploads_dir' => $_ENV['UPLOADS_DIR'],
            'displayErrorDetails' => (bool) $_ENV['ENVIRONMENT_DEV'],
            'logger' => [
                'name' => $_ENV['APP_LOGS_NAME'],
                'path' => '/../logs/app.log',
                'level' => Logger::DEBUG,
            ],
            'php-renderer' => [
                'templates_location' => __DIR__ . '/../templates'
            ],
            'doctrine' => [
                'mapping_files'   => [__DIR__ . '/xml-mapping/'],
                'entities'        => [__DIR__ . '/../src/Domain'],
                'database_params' => [
                    'driver'   => 'pdo_mysql',
                    'charset'  => 'utf8',
                    'port'     => '3306',
                    'collate'  => 'utf8mb4_general_ci',
                    'host'     => $_ENV['DATABASE_HOST'],
                    'dbname'   => $_ENV['DATABASE_NAME'],
                    'user'     => $_ENV['DATABASE_USER'],
                    'password' => $_ENV['DATABASE_PASS']
                ]
            ]
        ],
    ]);
};
