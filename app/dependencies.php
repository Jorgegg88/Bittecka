<?php
declare(strict_types=1);

use DI\ContainerBuilder;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Doctrine\ORM\Tools\Setup;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Slim\Views\PhpRenderer;

return static function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([

        LoggerInterface::class => static function (ContainerInterface $c) {
            $settings = $c->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        PhpRenderer::class => static function (ContainerInterface $c) {
            $settings = $c->get('settings');
            return new PhpRenderer($settings['php-renderer']['templates_location']);
        },

        EntityManager::class => static function (ContainerInterface $c) {

            $entitiesFiles = $c->get('settings')['doctrine']['entities'];
            $databaseParams = $c->get('settings')['doctrine']['database_params'];

            $config = Setup::createXMLMetadataConfiguration($entitiesFiles, $c->get('settings')['displayErrorDetails']);
            $entityManager = EntityManager::create($databaseParams, $config);

            $entityManager->getConfiguration()->setMetadataDriverImpl($c->get(XmlDriver::class));

            return $entityManager;
        },

        XmlDriver::class => static function (ContainerInterface $c) {
            return new XmlDriver($c->get('settings')['doctrine']['mapping_files']);
        }
    ]);
};
